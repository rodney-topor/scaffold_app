# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
ScaffoldApp::Application.config.secret_key_base = 'a204c24e80407f44e1c717d57f9e0862b1d7fed0fc61753d2e414cee82d9afe10ce84f0dc44dc8bed8eb0631615f21f9bc9e85162529179f6f5d5d1d30f960e4'
